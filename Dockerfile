FROM rust AS builder

WORKDIR /iventoy-autoiso
COPY . .

RUN apt-get update && apt-get install -y openssl && rm -rf /var/lib/apt/lists/*

RUN cargo install --path .


FROM ziggyds/iventoy:latest

RUN mkdir -p /root/.config/qBittorrent
RUN echo "[LegalNotice]" > /root/.config/qBittorrent/qBittorrent.conf
RUN echo "Accepted=true" >> /root/.config/qBittorrent/qBittorrent.conf

RUN apt-get update && apt-get install -y supervisor ca-certificates qbittorrent-nox && rm -rf /var/lib/apt/lists/*
COPY --from=builder /usr/local/cargo/bin/iventoy-autoiso /usr/local/bin/iventoy-autoiso

RUN mv /app/data/iventoy.dat /app/iventoy.dat

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf

RUN mkdir /app/external/{iso,cache} -p
RUN rm /app/iso -rf
RUN ln -s /app/external/iso /app/iso

ENTRYPOINT ["/usr/bin/supervisord"]