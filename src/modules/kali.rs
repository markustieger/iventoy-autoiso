use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, Url};
use crate::download;
use crate::modules::{CheckResult, DownloadMethod, IsoImage, Module, VersionFilenameCheckModule};
use crate::modules::Checksum::SHA256;

lazy_static! {
    static ref TORRENT_REGEX: Regex = Regex::new("<a href=\"(kali-linux-([0-9]+\\.[0-9]+)-([a-z]+)((?:-[a-z]+)?)-amd64.iso).torrent\">").unwrap();
    static ref CHECKSUM_REGEX: Regex = Regex::new("([a-f0-9]+)  (*\\.iso)").unwrap();
}

pub struct KaliLinuxModule {
    pub imagetype: KaliImage,
    pub edition: String,
    pub torrentonly: bool
}

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    for imagetype in vec![KaliImage::INSTALLER, KaliImage::LIVE] {
        available.push(Box::new(KaliLinuxModule {
            imagetype: imagetype.to_owned(),
            edition: String::from(""),
            torrentonly: false
        }));
        available.push(Box::new(KaliLinuxModule {
            imagetype: imagetype.to_owned(),
            edition: String::from("-everything"),
            torrentonly: true
        }));
    }
    available.push(Box::new(KaliLinuxModule {
        imagetype: KaliImage::INSTALLER,
        edition: String::from("-netinst"),
        torrentonly: false
    }));
}

impl VersionFilenameCheckModule for KaliLinuxModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for KaliLinuxModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://cdimage.kali.org/current/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in TORRENT_REGEX.captures_iter(response.as_str()) {
            let filename = captures.get(1).unwrap();
            let filename = filename.as_str();
            let version = captures.get(2).unwrap();
            let version = version.as_str();
            let imagetype = captures.get(3).unwrap();
            let imagetype = imagetype.as_str();
            let edition = captures.get(4).unwrap();
            let edition = edition.as_str();
            debug!("Version: {}, {}, {}, {}", filename, version, imagetype, edition);
            if edition.eq(self.edition.as_str()) && imagetype.eq(self.imagetype.fileprefix().as_str()) {
                versions.push((version.parse::<f32>()?, filename, version));
            }
        }
        versions.sort_by(|a, b| a.0.partial_cmp(&b.0).expect("Failed to order!").reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let first = versions.first().unwrap();
        let filename = String::from(first.1);
        let version = String::from(first.2);
        Ok(CheckResult {
            needs_download: self.needs_download(pool, filename.clone(), version.clone()).await?,
            version,
            properties: HashMap::from([
                (String::from("filename"), filename)
            ])
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP if self.torrentonly => {
                warn!("{} will use torrent instead of http, because http is not supported!", result.properties.get(&String::from("filename")).unwrap().clone());
                self.download(_pool, client, DownloadMethod::TORRENT, result).await
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://cdimage.kali.org/current/{}.torrent", result.properties.get(&String::from("filename")).unwrap()).parse()?,
                filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                checksum: None
            }),
            DownloadMethod::HTTP => {
                let mirror = env::var("KALI_MIRROR").unwrap_or(String::from("https://cdimage.kali.org/"));
                let checksum_url: Url = format!("{}SHA256SUMS", mirror).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    if filename.as_str().eq(result.properties.get(&String::from("filename")).unwrap()) {
                        checksum = Some(SHA256(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}{}", mirror, result.properties.get(&String::from("filename")).unwrap()).parse()?,
                    filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                    checksum
                })
            },
        }
    }

    fn get_name(&self) -> String {
        format!("KALILINUX-{}{}", self.imagetype.name().to_uppercase(), self.edition.to_uppercase())
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.properties.get(&String::from("filename")).unwrap().clone(), result.version, pool).await
    }
}

#[derive(PartialEq, Clone)]
pub enum KaliImage {
    INSTALLER, LIVE
}

impl KaliImage {

    fn fileprefix(&self) -> String {
        self.name()
    }

    fn path(&self) -> String {
        self.name()
    }

    fn name(&self) -> String {
        match self {
            KaliImage::INSTALLER => "installer",
            KaliImage::LIVE => "live"
        }.to_string()
    }

}