use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, Url};
use crate::modules::{CheckResult, DownloadMethod, IsoImage, Module, VersionFilenameCheckModule};
use crate::modules::Checksum::SHA256;

lazy_static! {
    static ref TORRENT_REGEX: Regex = Regex::new("<a href=\"(([a-z]+)-([0-9]*(?:\\.[0-9]+)?)-([a-z]+)-64bit(?:-v2)?\\.iso)\\.torrent\">").unwrap();
    static ref CHECKSUM_REGEX: Regex = Regex::new("([a-f0-9]+) \\*(*\\.iso)").unwrap();
}

pub struct LinuxMintModule {
    pub base: LinuxMintBase,
    pub edition: String
}

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    for base in vec![LinuxMintBase::LINUXMINT, LinuxMintBase::LMDE] {
        for edition in base.to_owned().editions() {
            available.push(Box::new(LinuxMintModule {
                base: base.to_owned(),
                edition
            }))
        }
    }
}

impl VersionFilenameCheckModule for LinuxMintModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for LinuxMintModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://linuxmint.com/torrents/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in TORRENT_REGEX.captures_iter(response.as_str()) {
            let filename = captures.get(1).unwrap();
            let filename = filename.as_str();
            let base = captures.get(2).unwrap();
            let base = base.as_str();
            let version = captures.get(3).unwrap();
            let version = version.as_str();
            let edition = captures.get(4).unwrap();
            let edition = edition.as_str();
            debug!("Version: {}, {}, {}, {}", filename, base, version, edition);
            if edition.eq(self.edition.as_str()) && base.eq(self.base.fileprefix().as_str()) {
                versions.push((version.parse::<f32>()?, filename, version));
            }
        }
        versions.sort_by(|a, b| a.0.partial_cmp(&b.0).expect("Failed to order!").reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let first = versions.first().unwrap();
        let filename = String::from(first.1);
        let version = String::from(first.2);
        Ok(CheckResult {
            needs_download: self.needs_download(pool, filename.clone(), version.clone()).await?,
            version,
            properties: HashMap::from([
                (String::from("filename"), filename)
            ])
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let mirror = env::var("LINUXMINT_MIRROR").unwrap_or(String::from("https://mirrors.kernel.org/linuxmint/"));
                let mut version_delegation = String::new();
                if self.base == LinuxMintBase::LINUXMINT {
                    version_delegation = format!("/{}", result.version);
                }
                let checksum_url: Url = format!("{}iso/{}{}/sha256sum.txt", mirror, self.base.path(), version_delegation).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    if filename.as_str().eq(result.properties.get(&String::from("filename")).unwrap()) {
                        checksum = Some(SHA256(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}iso/{}{}/{}", mirror, self.base.path(), version_delegation, result.properties.get(&String::from("filename")).unwrap()).parse()?,
                    filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://linuxmint.com/torrents/{}.torrent", result.properties.get(&String::from("filename")).unwrap()).parse()?,
                filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        format!("{}-{}", self.base.name().to_uppercase(), self.edition.to_uppercase())
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.properties.get(&String::from("filename")).unwrap().clone(), result.version, pool).await
    }
}

#[derive(PartialEq, Clone)]
pub enum LinuxMintBase {
    LINUXMINT, LMDE
}

impl LinuxMintBase {

    fn fileprefix(&self) -> String {
        self.name()
    }

    fn path(&self) -> String {
        match self {
            LinuxMintBase::LINUXMINT => "stable",
            LinuxMintBase::LMDE => "debian"
        }.to_string()
    }

    fn name(&self) -> String {
        match self {
            LinuxMintBase::LINUXMINT => "linuxmint",
            LinuxMintBase::LMDE => "lmde"
        }.to_string()
    }

    fn editions(&self) -> Vec<String> {
        match self {
            LinuxMintBase::LINUXMINT => vec!["cinnamon", "mate", "xfce"],
            LinuxMintBase::LMDE => vec!["cinnamon"]
        }.iter().map(|str| String::from(*str)).collect()
    }

}