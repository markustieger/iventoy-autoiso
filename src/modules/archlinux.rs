use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, Url};
use crate::modules::{CheckResult, DownloadMethod, IsoImage, Module, VersionCheckModule, VersionFilenameCheckModule};
use crate::modules::Checksum::SHA256;

lazy_static! {
    static ref VERSION_REGEX: Regex = Regex::new("<td><a href=\"/releng/releases/([0-9]+\\.[0-9]+\\.[0-9]+)/\" title=\"(?:.*)\">").unwrap();
    static ref CHECKSUM_REGEX: Regex = Regex::new("([a-f0-9]+)  (*\\.iso)").unwrap();
}

pub struct ArchLinuxModule;

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    available.push(Box::new(ArchLinuxModule));
}

impl VersionCheckModule for ArchLinuxModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for ArchLinuxModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://archlinux.org/releng/releases/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in VERSION_REGEX.captures_iter(response.as_str()) {
            let version = captures.get(1).unwrap();
            let version = version.as_str();
            debug!("Version: {}", version);
            versions.push(version);
        }
        versions.sort_by(|a, b| a.cmp(b).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let version = String::from(versions.first().unwrap().to_owned());
        Ok(CheckResult {
            needs_download: self.needs_download(pool, version.clone()).await?,
            version,
            properties: HashMap::new()
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let mirror = env::var("ARCHLINUX_MIRROR").unwrap_or(String::from("https://mirrors.edge.kernel.org/archlinux/"));
                let checksum_url: Url = format!("{}iso/{}/sha256sums.txt", mirror, result.version).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    if filename.as_str().eq(format!("archlinux-{}-x86_64.iso", result.version).as_str()) {
                        checksum = Some(SHA256(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}iso/{}/archlinux-{}-x86_64.iso", mirror, result.version, result.version).parse()?,
                    filename: format!("archlinux-{}-x86_64.iso", result.version),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://archlinux.org/releng/releases/{}/torrent/", result.version).parse()?,
                filename: format!("archlinux-{}-x86_64.iso", result.version),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        String::from("ARCHLINUX")
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.version, pool).await
    }
}