use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, Url};
use crate::modules::{CheckResult, DownloadMethod, IsoImage, Module, VersionFilenameCheckModule};
use crate::modules::Checksum::SHA512;

lazy_static! {
    static ref LIVE_REGEX: Regex = Regex::new("<a href=\"(debian-live-([0-9]+\\.[0-9]+\\.[0-9]+)-amd64-([a-z]+)\\.iso)\">").unwrap();
    static ref INSTALLER_REGEX: Regex = Regex::new("<a href=\"(debian-([0-9]+\\.[0-9]+\\.[0-9]+)-amd64-netinst\\.iso)\">").unwrap();
    static ref CHECKSUM_REGEX: Regex = Regex::new("([a-f0-9]+)  (*\\.iso)").unwrap();
}

pub struct DebianLiveModule {
    pub desktop: String
}

pub struct DebianInstallerModule;

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    for desktop in vec!["standard", "cinnamon", "gnome", "kde", "lxde", "lxqt", "mate", "xfce"] {
        available.push(Box::new(DebianLiveModule {
            desktop: desktop.to_string()
        }));
    }
    available.push(Box::new(DebianInstallerModule));
}

impl VersionFilenameCheckModule for DebianLiveModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

impl VersionFilenameCheckModule for DebianInstallerModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for DebianInstallerModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in INSTALLER_REGEX.captures_iter(response.as_str()) {
            let filename = captures.get(1).unwrap();
            let filename = filename.as_str();
            let version = captures.get(2).unwrap();
            let version = version.as_str();
            debug!("Version: {}, {}", filename, version);
            versions.push((filename, version));
        }
        versions.sort_by(|a, b| a.1.cmp(b.1).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let first = versions.first().unwrap();
        let filename = String::from(first.0);
        let version = String::from(first.1);
        Ok(CheckResult {
            needs_download: self.needs_download(pool, filename.clone(), version.clone()).await?,
            version,
            properties: HashMap::from([
                (String::from("filename"), filename)
            ])
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let mirror = env::var("DEBIAN_MIRROR").unwrap_or(String::from("https://cdimage.debian.org/"));
                let checksum_url: Url = format!("{}debian-cd/current/amd64/iso-cd/SHA512SUMS", mirror).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    if filename.as_str().eq(result.properties.get(&String::from("filename")).unwrap()) {
                        checksum = Some(SHA512(String::from(hash.as_str())));
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}debian-cd/current/amd64/iso-cd/{}", mirror, result.properties.get(&String::from("filename")).unwrap()).parse()?,
                    filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://cdimage.debian.org/debian-cd/current/amd64/bt-cd/{}.torrent", result.properties.get(&String::from("filename")).unwrap()).parse()?,
                filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        String::from("DEBIAN-INSTALLER")
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.properties.get(&String::from("filename")).unwrap().clone(), result.version, pool).await
    }
}

#[async_trait]
impl Module for DebianLiveModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in LIVE_REGEX.captures_iter(response.as_str()) {
            let filename = captures.get(1).unwrap();
            let filename = filename.as_str();
            let version = captures.get(2).unwrap();
            let version = version.as_str();
            let desktop = captures.get(3).unwrap();
            let desktop = desktop.as_str();
            debug!("Version: {}, {}, {}", filename, version, desktop);
            if desktop.eq(self.desktop.as_str()) {
                versions.push((filename, version));
            }
        }
        versions.sort_by(|a, b| a.1.cmp(b.1).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let first = versions.first().unwrap();
        let filename = String::from(first.0);
        let version = String::from(first.1);
        Ok(CheckResult {
            needs_download: self.needs_download(pool, filename.clone(), version.clone()).await?,
            version,
            properties: HashMap::from([
                (String::from("filename"), filename)
            ])
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let mirror = env::var("DEBIAN_MIRROR").unwrap_or(String::from("https://cdimage.debian.org/"));
                let checksum_url: Url = format!("{}debian-cd/current-live/amd64/iso-hybrid/SHA512SUMS", mirror).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    if filename.as_str().eq(result.properties.get(&String::from("filename")).unwrap()) {
                        checksum = Some(SHA512(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}debian-cd/current-live/amd64/iso-hybrid/{}", mirror, result.properties.get(&String::from("filename")).unwrap()).parse()?,
                    filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://cdimage.debian.org/debian-cd/current-live/amd64/bt-hybrid/{}.torrent", result.properties.get(&String::from("filename")).unwrap()).parse()?,
                filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        format!("DEBIAN-LIVE-{}", self.desktop.to_uppercase())
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.properties.get(&String::from("filename")).unwrap().clone(), result.version, pool).await
    }
}