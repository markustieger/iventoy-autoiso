use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, Url};
use crate::modules::{CheckResult, DownloadMethod, IsoImage, Module, VersionCheckModule, VersionFilenameCheckModule};
use crate::modules::Checksum::{SHA256};

lazy_static! {
    static ref VERSION_REGEX: Regex = Regex::new("<a href=\"([0-9]+\\.[0-9]+(\\.[0-9]+)?)/\">").unwrap();
    static ref CHECKSUM_REGEX: Regex = Regex::new("([a-f0-9]+) \\*(*\\.iso)").unwrap();
}

pub struct UbuntuFlavourModule {
    pub flavour: String
}

pub struct UbuntuModule;

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    available.push(Box::new(UbuntuModule));
    for flavour in vec!["edubuntu", "kubuntu", "lubuntu", "ubuntu-budgie", "ubuntu-mate", "ubuntu-unity", "xubuntu", "ubuntukylin"] {
        available.push(Box::new(UbuntuFlavourModule {
            flavour: flavour.to_string()
        }));
    }
}

impl VersionCheckModule for UbuntuModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for UbuntuModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get("https://releases.ubuntu.com/").send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in VERSION_REGEX.captures_iter(response.as_str()) {
            let version = captures.get(1).unwrap();
            let version = version.as_str();
            debug!("Version: {}", version);
            versions.push(version);
        }
        versions.sort_by(|a, b| a.cmp(b).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let version = String::from(versions.first().unwrap().to_owned());
        Ok(CheckResult {
            needs_download: self.needs_download(pool, version.clone()).await?,
            version,
            properties: HashMap::new()
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let checksum_url: Url = format!("https://releases.ubuntu.com/{}/SHA256SUMS", result.version).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    let filename = filename.as_str();
                    if filename.eq(format!("ubuntu-{}-desktop-amd64.iso", result.version).as_str()) {
                        checksum = Some(SHA256(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("https://releases.ubuntu.com/{}/ubuntu-{}-desktop-amd64.iso", result.version, result.version).parse()?,
                    filename: format!("ubuntu-{}-desktop-amd64.iso", result.version),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://releases.ubuntu.com/{}/ubuntu-{}-desktop-amd64.iso.torrent", result.version, result.version).parse()?,
                filename: format!("ubuntu-{}-desktop-amd64.iso", result.version),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        format!("UBUNTU")
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.version, pool).await
    }
}

impl VersionCheckModule for UbuntuFlavourModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for UbuntuFlavourModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get(format!("https://cdimage.ubuntu.com/{}/releases/", self.flavour)).send().await?;
        let response = response.text().await?;
        let mut versions = vec![];
        for captures in VERSION_REGEX.captures_iter(response.as_str()) {
            let version = captures.get(1).unwrap();
            let version = version.as_str();
            debug!("Version: {}", version);
            versions.push(version);
        }
        versions.sort_by(|a, b| a.cmp(b).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let version = String::from(versions.first().unwrap().to_owned());
        Ok(CheckResult {
            needs_download: self.needs_download(pool, version.clone()).await?,
            version,
            properties: HashMap::new()
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let mirror = env::var("UBUNTU_CDIMAGE_MIRROR").unwrap_or(String::from("https://cdimage.ubuntu.com/"));
                let checksum_url: Url = format!("{}{}/releases/{}/release/SHA256SUMS", mirror, self.flavour, result.version).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;
                let mut checksum = None;
                for captures in CHECKSUM_REGEX.captures_iter(&response) {
                    let hash = captures.get(1).unwrap();
                    let filename = captures.get(2).unwrap();
                    let filename = filename.as_str();
                    if filename.eq(format!("{}-{}-desktop-amd64.iso", self.flavour, result.version).as_str()) {
                        checksum = Some(SHA256(String::from(hash.as_str())));
                        break;
                    }
                }
                if checksum.is_none() {
                    warn!("Failed to get checksums. Ignoring...");
                }

                Ok(IsoImage {
                    method,
                    url: format!("{}{}/releases/{}/release/{}-{}-desktop-amd64.iso", mirror, self.flavour, result.version, self.flavour, result.version).parse()?,
                    filename: format!("{}-{}-desktop-amd64.iso", self.flavour, result.version),
                    checksum
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://cdimage.ubuntu.com/{}/releases/{}/release/{}-{}-desktop-amd64.iso.torrent", self.flavour, result.version, self.flavour, result.version).parse()?,
                filename: format!("{}-{}-desktop-amd64.iso", self.flavour, result.version),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        format!("UBUNTU-FLAVOUR-{}", self.flavour.to_uppercase())
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.version, pool).await
    }
}