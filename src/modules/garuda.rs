use std::error::Error;
use std::{env, io};
use std::collections::HashMap;
use std::io::ErrorKind;
use async_trait::async_trait;
use lazy_static::lazy_static;
use log::{debug, warn};
use mysql::Pool;
use regex::Regex;
use reqwest::{Client, ClientBuilder, Url};
use reqwest::redirect::Policy;
use crate::modules::{CheckResult, Checksum, DownloadMethod, IsoImage, Module, VersionCheckModule, VersionFilenameCheckModule};
use crate::modules::Checksum::SHA256;

lazy_static! {
    static ref URL_REGEX: Regex = Regex::new("(?:.*)/(garuda-([a-z]+)-linux-zen-([0-9]+).iso)").unwrap();
    static ref VERSION_REGEX: Regex = Regex::new("<a href=\"([0-9]+)/\">").unwrap();
}

pub struct GarudaModule {
    pub maintainer: GarudaMaintainer,
    pub desktop: String
}

pub fn register(available: &mut Vec<Box<dyn Module>>) {
    for maintainer in vec![GarudaMaintainer::GARUDA, GarudaMaintainer::COMMUNITY] {
        for desktop in maintainer.to_owned().desktops() {
            available.push(Box::new(GarudaModule {
                maintainer: maintainer.to_owned(),
                desktop: desktop.clone()
            }));
        }
    }
}

impl VersionFilenameCheckModule for GarudaModule {

    fn get_name0(&self) -> String {
        self.get_name()
    }

}

#[async_trait]
impl Module for GarudaModule {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.setup0(pool).await
    }

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>> {
        let response = client.get(format!("https://iso.builds.garudalinux.org/iso/latest/{}/{}/latest.iso", self.maintainer.path(), self.desktop)).send().await?;
        let location = response.headers().get("location");
        if location.is_none() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No location header could be found!")));
        }
        let location = location.unwrap();
        let mut versions = vec![];
        for captures in VERSION_REGEX.captures_iter(location.to_str().unwrap()) {
            let filename = captures.get(1).unwrap();
            let filename = filename.as_str();
            let desktop = captures.get(2).unwrap();
            let desktop = desktop.as_str();
            let version = captures.get(3).unwrap();
            let version = version.as_str();
            debug!("Version: {}, {}, {}", filename, desktop, version);
            if desktop.eq(self.desktop.as_str()) {
                versions.push((filename, version));
            }
        }
        versions.sort_by(|a, b| a.1.cmp(b.1).reverse());
        if versions.is_empty() {
            return Err(Box::new(io::Error::new(ErrorKind::NotFound, "No version could be found!")));
        }

        let (filename, version) = versions.first().unwrap();
        let filename = String::from(filename.to_owned());
        let version = String::from(version.to_owned());
        Ok(CheckResult {
            needs_download: self.needs_download(pool, filename.clone(), version.clone()).await?,
            version,
            properties: HashMap::from([
                (String::from("filename"), filename)
            ])
        })
    }

    async fn download(&self, _pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>> {
        match method {
            DownloadMethod::HTTP => {
                let checksum_url: Url = format!("https://iso.builds.garudalinux.org/iso/{}/{}/{}/garuda-{}-linux-zen-{}.iso.sha256", self.maintainer.path(), self.desktop, result.version, self.desktop, result.version).parse()?;

                let response = client.get(checksum_url).send().await?;
                let response = response.text().await?;

                Ok(IsoImage {
                    method,
                    url: format!("https://iso.builds.garudalinux.org/iso/{}/{}/{}/garuda-{}-linux-zen-{}.iso", self.maintainer.path(), self.desktop, result.version, self.desktop, result.version).parse()?,
                    filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                    checksum: Some(Checksum::SHA256(response))
                })
            },
            DownloadMethod::TORRENT => Ok(IsoImage {
                method,
                url: format!("https://iso.builds.garudalinux.org/iso/{}/{}/{}/garuda-{}-linux-zen-{}.iso.torrent", self.maintainer.path(), self.desktop, result.version, self.desktop, result.version).parse()?,
                filename: result.properties.get(&String::from("filename")).unwrap().clone(),
                checksum: None
            }),
        }
    }

    fn get_name(&self) -> String {
        format!("GARUDA-{}", self.desktop.to_uppercase())
    }

    fn create_client(&self, builder: ClientBuilder) -> reqwest::Result<Client> {
        builder.redirect(Policy::none()).build()
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>> {
        self.finalize0(result.properties.get(&String::from("filename")).unwrap().clone(), result.version, pool).await
    }
}

#[derive(PartialEq, Clone)]
pub enum GarudaMaintainer {
    GARUDA, COMMUNITY
}

impl GarudaMaintainer {

    fn path(&self) -> String {
        match self {
            GarudaMaintainer::GARUDA => "garuda",
            GarudaMaintainer::COMMUNITY => "community"
        }.to_string()
    }

    fn desktops(&self) -> Vec<String> {
        match self {
            GarudaMaintainer::GARUDA => vec!["cinnamon", "dr460nized", "dr460nized-gaming", "gnome", "i3", "kde-git", "kde-lite", "lxqt-kwin", "mate", "qtile", "sway", "wayfire", "xfce"],
            GarudaMaintainer::COMMUNITY => vec![]
        }.iter().map(|str| String::from(*str)).collect()
    }

}