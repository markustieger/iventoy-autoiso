INSERT INTO `files`
(
 `module`,
 `filename`
)
VALUES
(
 :module,
 :filename
)
ON DUPLICATE KEY
UPDATE
`filename` = :filename