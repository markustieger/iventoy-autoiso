INSERT INTO `entries`
(
 `module`,
 `filename`,
 `version`
)
VALUES
(
 :module,
 :filename,
 :version
)
ON DUPLICATE KEY
UPDATE
`filename` = :filename,
`version` = :version