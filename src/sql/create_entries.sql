CREATE TABLE IF NOT EXISTS `entries`
(
    `module` VARCHAR(255) NOT NULL PRIMARY KEY,
    `filename` VARCHAR(255),
    `version` VARCHAR(255)
)