
use std::collections::HashMap;
use std::error::Error;

use std::io;
use std::io::ErrorKind;
use std::str::FromStr;
use async_trait::async_trait;
use mysql::{Params, Pool, Row, Value};
use mysql::prelude::Queryable;
use reqwest::{Client, ClientBuilder, Url};

pub mod linuxmint;
pub mod debian;
pub mod ubuntu;
pub mod archlinux;
pub mod garuda;
pub mod kali;

#[async_trait]
pub trait Module {

    async fn setup(&self, pool: &Pool) -> Result<(), Box<dyn Error>>;

    async fn check(&self, client: &Client, pool: &Pool) -> Result<CheckResult, Box<dyn Error>>;

    async fn download(&self, pool: &Pool, client: &Client, method: DownloadMethod, result: CheckResult) -> Result<IsoImage, Box<dyn Error>>;

    fn get_name(&self) -> String;

    fn create_client(&self, builder: ClientBuilder) -> reqwest::Result<Client> {
        builder.build()
    }

    async fn finalize(&self, result: CheckResult, pool: &Pool) -> Result<(), Box<dyn Error>>;
}

pub struct IsoImage {
    pub method: DownloadMethod,
    pub url: Url,
    pub filename: String,
    pub checksum: Option<Checksum>
}

pub enum Checksum {
    SHA256(String),
    SHA512(String),
}

#[derive(Clone)]
pub struct CheckResult {
    pub needs_download: bool,
    pub version: String,
    pub properties: HashMap<String, String>
}

#[derive(Clone)]
pub enum DownloadMethod {
    HTTP, TORRENT
}

impl ToString for DownloadMethod {
    fn to_string(&self) -> String {
        String::from(match self {
            DownloadMethod::HTTP => "http",
            DownloadMethod::TORRENT => "torrent"
        })
    }
}

impl FromStr for DownloadMethod {
    type Err = io::Error;

    fn from_str(s: &str) -> Result<Self, io::Error> {
        Ok(match s.to_lowercase().as_str() {
            "http" => DownloadMethod::HTTP,
            "torrent" => DownloadMethod::TORRENT,
            _ => return Err(io::Error::new(ErrorKind::NotFound, "Value wasn't found!"))
        })
    }
}

#[async_trait]
pub trait VersionCheckModule {

    async fn setup0(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        conn.query_drop(include_str!("sql/create_entries.sql"))?;
        Ok(())
    }

    async fn finalize0(&self, version: String, pool: &Pool) -> Result<(), Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        let stmt = conn.prep(include_str!("sql/insert_entries.sql"))?;
        conn.exec_drop(stmt, Params::Named(
            HashMap::from(
                [
                    (String::from("module").into_bytes(), Value::from(self.get_name0())),
                    (String::from("filename").into_bytes(), Value::NULL),
                    (String::from("version").into_bytes(), Value::from(version))
                ]
            )
        ))?;
        Ok(())
    }

    fn get_name0(&self) -> String;

    async fn needs_download(&self, pool: &Pool, version: String) -> Result<bool, Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        let stmt = conn.prep(include_str!("sql/select_entries.sql"))?;
        let result: Option<Row> = conn.exec_first(stmt, Params::Positional(vec![Value::from(self.get_name0())]))?;

        match result {
            None => Ok(true),
            Some(result) => Ok(!version.eq(result.get::<String, &str>("version").unwrap().as_str()))
        }
    }

}

#[async_trait]
pub trait VersionFilenameCheckModule {

    async fn setup0(&self, pool: &Pool) -> Result<(), Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        conn.query_drop(include_str!("sql/create_entries.sql"))?;
        Ok(())
    }

    async fn finalize0(&self, filename: String, version: String, pool: &Pool) -> Result<(), Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        let stmt = conn.prep(include_str!("sql/insert_entries.sql"))?;
        conn.exec_drop(stmt, Params::Named(
            HashMap::from(
                [
                    (String::from("module").into_bytes(), Value::from(self.get_name0())),
                    (String::from("filename").into_bytes(), Value::from(filename)),
                    (String::from("version").into_bytes(), Value::from(version))
                ]
            )
        ))?;
        Ok(())
    }

    fn get_name0(&self) -> String;

    async fn needs_download(&self, pool: &Pool, filename: String, version: String) -> Result<bool, Box<dyn Error>> {
        let mut conn = pool.get_conn()?;
        let stmt = conn.prep(include_str!("sql/select_entries.sql"))?;
        let result: Option<Row> = conn.exec_first(stmt, Params::Positional(vec![Value::from(self.get_name0())]))?;

        match result {
            None => Ok(true),
            Some(result) => Ok(
                !filename.eq(result.get::<String, &str>("filename").unwrap().as_str()) ||
                    !version.eq(result.get::<String, &str>("version").unwrap().as_str()))
        }
    }

}