mod modules;


use tokio_util::compat::FuturesAsyncReadCompatExt;



use std::str::FromStr;


use std::{env, fs, io};
use std::collections::HashMap;
use std::fs::File;






use std::time::{Duration, SystemTime};
use futures::{TryStreamExt};
use lazy_static::lazy_static;
use log::{debug, error, info, LevelFilter, warn};
use mysql::{Params, Pool};
use mysql::Value;
use mysql::prelude::{Queryable};
use qbit_rs::model::{AddTorrentArg, Credential, GetTorrentListArg, Hashes, Sep, State, TorrentSource};
use qbit_rs::Qbit;
use regex::Regex;


use reqwest::{Client, ClientBuilder};
use sha2::{Sha256, Sha512, Digest};
use simplelog::{ColorChoice, Config, TerminalMode, TermLogger};
use crate::modules::{CheckResult, Checksum, DownloadMethod, IsoImage, Module};

static ISOPATH: &str = "/app/external/iso/";
static CACHEPATH: &str = "/app/external/cache/";

lazy_static!(
    static ref SHA256: Sha256 = Sha256::new();
    static ref SHA512: Sha512 = Sha512::new();
    static ref QBIT: Qbit = qbit_rs::Qbit::new("http://127.0.0.1:8080", Credential::new("admin", "adminadmin"));
);

fn main() {
    let rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(app());
}

async fn app() {
    TermLogger::init(
        LevelFilter::from_str(
            env::var("AUTOISO_LOGLEVEL")
            .unwrap_or(LevelFilter::Info.to_string())
                .as_str()).expect("Failed to convert environment variable to loglevel!"),
        Config::default(),
        TerminalMode::Stdout,
        ColorChoice::Auto)
        .expect("Failed to initialize logger!");

    info!("Starting iVentoy AutoISO Daemon... (Made by MarkusTieger)");

    info!("Delaying for qBittorrent startup...");
    tokio::time::sleep(Duration::from_secs(30)).await;

    info!("Preparing...");
    let _ = std::fs::remove_dir_all(CACHEPATH);
    std::fs::create_dir_all(ISOPATH).expect("Failed to mkdir the iso directory!");
    std::fs::create_dir_all(CACHEPATH).expect("Failed to mkdir the cache directory!");

    let mut available: Vec<Box<dyn Module>> = vec![];

    // Register your modules here
    modules::linuxmint::register(&mut available);
    modules::debian::register(&mut available);
    modules::ubuntu::register(&mut available);
    modules::archlinux::register(&mut available);
    modules::garuda::register(&mut available);
    modules::kali::register(&mut available);
    if env::var("ALLOW_EXPERIMENTAL_MODULES").unwrap_or(false.to_string()).parse().expect("Failed to parse environment variable") {
    }

    let mut modules: Vec<Box<dyn Module>> = vec![];
    let mut regrets: Vec<(Regex, bool)> = vec![];

    for (name, value) in env::vars() {
        if name.starts_with("MODULE_") {
            regrets.push(
                (Regex::new(&name.as_str()[7..]).expect("Failed to parse environment variable"),
                 value.parse().expect("Failed to parse environment variable"))
            );
        }
    }

    for module in available {
        for (_regex, value) in &regrets {
            if value.clone() {
                modules.push(module);
                break;
            }
        }
    }

    for module in &modules {
        info!("Enabled module: {}", module.get_name());
    }

    info!("Connecting to mysql...");
    let url = env::var("mysql").expect("\"mysql\" environment variable missing!");
    let pool = Pool::new(url.as_str()).expect("Couldn't create mysql pool!");

    info!("Setting up daemon...");
    let mut conn = pool.get_conn().expect("Failed to establish connection!");
    conn.query_drop(include_str!("sql/create_files.sql")).expect("Failed to create table for finalization!");
    drop(conn);

    for module in &modules {
        info!("Setting up module {}", module.get_name());
        module.setup(&pool).await.expect("Failed to setup module! Try disabling it");
    }

    let mut time = SystemTime::UNIX_EPOCH;
    let duration: u64 = env::var("CHECK_DURATION").unwrap_or(String::from("1800")).parse().expect("Failed to parse environment variable");
    let method: DownloadMethod = env::var("DOWNLOAD_METHOD")
        .unwrap_or(DownloadMethod::TORRENT.to_string())
        .parse().expect("Failed to parse download method!");

    loop {
        if time.elapsed().unwrap().as_secs() > duration {
            check(method.to_owned(), &pool, &modules).await;
            time = SystemTime::now();
        }
    }
}

async fn check(method: DownloadMethod, pool: &Pool, enabled: &Vec<Box<dyn Module>>) {
    for module in enabled {
        let client = ClientBuilder::new()
            .http1_title_case_headers();
        let client = module.create_client(client).expect("Failed to build client!");
        match module.check(&client, pool).await {
            Ok(result) => {
                if !result.needs_download {
                    debug!("No update for {} needed!", module.get_name());
                    continue;
                }
                info!("New Version available for {}: {}", module.get_name(), result.version);
                let image = match module.download(pool, &client, method.clone(), result.clone()).await {
                    Ok(image) => image,
                    Err(err) => {
                        warn!("Failed to download on module {}. This module will be skipped! {:?}", module.get_name(), err);
                        continue;
                    }
                };
                download(module, result, image, &client, pool).await;
            }
            Err(err) => {
                warn!("Failed to check on module {}. This module will be skipped! {:?}", module.get_name(), err);
                continue;
            }
        }
    }
}

async fn download(module: &Box<dyn Module>, result: CheckResult, image: IsoImage, client: &Client, pool: &Pool) {
    info!("Downloading {}... from {} ({}) using {}", image.filename, image.url, module.get_name(), image.method.to_string());
    let path = match image.method {
        DownloadMethod::HTTP => {
            let filepath = format!("{}{}", CACHEPATH, uuid::Uuid::new_v4().to_string());
            let mut file = tokio::fs::File::create(&filepath).await.expect("Failed to open file");

            match client.get(image.url.clone()).send().await {
                Ok(response) => {
                    let stream = response.bytes_stream();
                    let stream = stream
                        .map_err(|e| futures::io::Error::new(futures::io::ErrorKind::Other, e))
                        .into_async_read();
                    let mut stream = stream.compat();
                    match tokio::io::copy(&mut stream, &mut file).await {
                        Ok(count) => {
                            let file = std::fs::File::open(&filepath).expect("Failed to open file");
                            let meta = file.metadata().expect("Failed to receive filemeta");
                            if meta.len() != count {
                                let _ = std::fs::remove_file(&filepath);
                                warn!("Length mismatched! Server: {}, Local: {}. This module will be skipped!", count, meta.len());
                                return;
                            }
                            filepath
                        }
                        Err(_) => {
                            let _ = std::fs::remove_file(&filepath);
                            warn!("Failed to download from {}. This module will be skipped!", image.url);
                            return;
                        }
                    }
                }
                Err(_) => {
                    let _ = std::fs::remove_file(&filepath);
                    warn!("Failed to download from {}. This module will be skipped!", image.url);
                    return;
                }
            }
        }
        DownloadMethod::TORRENT => {
            QBIT.delete_torrents(Hashes::All, Some(true)).await.expect("Failed to delete all torrents!");

            let mut name = String::new();
            let mut torrent = AddTorrentArg::default();
            torrent.source = TorrentSource::Urls { urls: Sep::from(vec![image.url])};
            torrent.savepath = Some(CACHEPATH.to_string());

            QBIT.add_torrent(torrent).await.expect("Failed to add the torrent!");
            loop {
                tokio::time::sleep(Duration::from_secs(10)).await;
                let torrents = QBIT.get_torrent_list(GetTorrentListArg::default()).await.expect("Failed to retrieve lists of torrents");
                if torrents.len() != 1 {
                    error!("The list of torrents is not 1! {:?}", torrents);
                    return;
                }
                let torrent = torrents.get(0).unwrap();
                name = torrent.name.as_ref().unwrap().clone();
                if torrent.state.is_some() {
                    let state = torrent.state.as_ref().unwrap().clone();
                    match state {
                        State::Allocating | State::MetaDL | State::Downloading | State::StalledDL | State::CheckingDL | State::CheckingUP | State::QueuedDL | State::CheckingResumeData | State::Moving => {},
                        State::Uploading | State::StalledUP => {
                            QBIT.delete_torrents(Hashes::All, Some(false))
                                .await.expect("Failed to delete all torrents!");
                            break;
                        },
                        _ => {
                            warn!("Unknown state: {:?}. Will be handled as error!", state);
                            return;
                        },
                    }
                }
            }

            format!("{}{}", CACHEPATH, name)
        }
    };

    if let Some(checksum) = image.checksum {
        let mut file = File::open(&path).expect("Failed to open file for checksum verification!");
        if let Checksum::SHA256(hex) = checksum {
            let mut sha256_hasher = SHA256.to_owned();
            io::copy(&mut file, &mut sha256_hasher).expect("Failed to update hasher for checksum!");
            let local = format!("{:x}", sha256_hasher.finalize());
            if !local.eq_ignore_ascii_case(&hex) {
                let _ = fs::remove_file(&path);
                warn!("Checksum mismatched! Server: {}, Local: {}. This module will be skipped!", hex, local);
                return;
            }
        } else if let Checksum::SHA512(hex) = checksum {
            let mut sha512_hasher = SHA512.to_owned();
            io::copy(&mut file, &mut sha512_hasher).expect("Failed to update hasher for checksum!");
            let local = format!("{:x}", sha512_hasher.finalize());
            if !local.eq_ignore_ascii_case(&hex) {
                let _ = fs::remove_file(&path);
                warn!("Checksum mismatched! Server: {}, Local: {}. This module will be skipped!", hex, local);
                return;
            }
        }
    }

    fs::rename(path, format!("{}{}", ISOPATH, image.filename)).expect("Failed to rename file. That means the iso location is probably on another mount than the cache location. The daemon will crash now!");

    let mut conn = pool.get_conn().expect("Failed to establish connection!");
    let stmt = conn.prep(include_str!("sql/insert_files.sql")).expect("Failed to prepare Statement for finalization!");

    conn.exec_drop(stmt, Params::Named(
        HashMap::from(
            [
                (String::from("module").into_bytes(), Value::from(module.get_name())),
                (String::from("filename").into_bytes(), Value::from(image.filename))
            ]
        )
    )).expect("Failed to execute Statement for finalization!");
    drop(conn);

    module.finalize(result, pool).await.expect("Failed to execute module finalization!");

    info!("Download was successfully");

    // Refreshing image list in iventoy
    let request = client.post("http://127.0.0.1:26000/iventoy/json")
        .json(&HashMap::from([("method", "refresh_img_list")])).build().expect("Failed to build request!");
    match client.execute(request).await {
        Ok(response) => {
            let url = response.url().clone();
            debug!("Response from iVentoy on refresh: {}, URL: {}", response.text().await.expect("Failed to get text of response!"), url.as_str())
        },
        Err(err) => warn!("Failed to refresh image list in iVentoy! Ignoring... {:?}", err)
    }

}