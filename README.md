## iVentoy-Autoiso
Is a daemon that automatically downloads the latest isos from various distros and checks for updates.
I included a Dockerfile, which is currently the only supported way of using it.

